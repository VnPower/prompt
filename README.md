# prompt

A small library for selection lists, in a file with no dependency.

## Installation

You can just copy the source file (`__init__.py`) right into your project.

Or, you can use Git submodules instead:

```
git submodule add https://codeberg.org/VnPower/prompt.git prompt/
```

This is a library I originally made for personal use, so I don't want to publish
it on PYPI.
